from django.urls import path
from . import views

app_name = 'Registerbusiness'
urlpatterns = [

    path('', views.dashboard, name='dashboard'),
    path('register/', views.register_business, name='register_business'),
    path('user_logout/',views.user_logout,name= "logout"),
    path('login/', views.business_owner_login, name='business_owner_login'),
    path('products/add/', views.add_product, name='add_product'),
    path('business_home/',views.business_home, name = 'business_home'),
   
]
