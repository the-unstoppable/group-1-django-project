from django.apps import AppConfig


class RegisterbusinessConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Registerbusiness'
