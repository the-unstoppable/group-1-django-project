from django import forms
from Connectify.models import Category
from .models import BusinessOwner,Product
from django.contrib.auth import authenticate

class BusinessSignUpForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)  # Add password field explicitly

    class Meta:
        model = BusinessOwner
        fields = ['name', 'description', 'contact_info', 'contact_email', 'contact_phone', 'password']


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ['owner', 'name', 'description', 'price', 'image', 'inventory_count', 'category']

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        self.fields['category'].queryset = Category.objects.all()  # Populate category choices
        
class BusinessLogInForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(widget=forms.PasswordInput)

    def clean(self):
        cleaned_data = super().clean()
        username = cleaned_data.get('username')
        password = cleaned_data.get('password')

        if username and password:
            user = authenticate(username=username, password=password)
            if user is None or not user.is_active:
                raise forms.ValidationError("Invalid username or password.")

        return cleaned_data