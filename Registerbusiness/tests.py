from django.test import TestCase
from Connectify.models import Category
# Create your tests here.from django.test import TestCase
from django.urls import reverse
from .models import BusinessOwner, Product
from .forms import BusinessSignUpForm, ProductForm, BusinessLogInForm


class BusinessOwnerModelTest(TestCase):
    def setUp(self):
        self.owner = BusinessOwner.objects.create(
            name='Test Owner',
            description='Test Description',
            contact_email='test@example.com',
            contact_phone='1234567890',
            contact_info='Test Contact Info',
            password='testpassword'
        )

    def test_owner_name(self):
        self.assertEqual(str(self.owner), 'Test Owner')


class ProductModelTest(TestCase):
    def setUp(self):
        self.owner = BusinessOwner.objects.create(
            name='Test Owner',
            description='Test Description',
            contact_email='test@example.com',
            contact_phone='1234567890',
            contact_info='Test Contact Info',
            password='testpassword'
        )
        self.product = Product.objects.create(
            owner=self.owner,
            name='Test Product',
            description='Test Description',
            price=10.0,
            image='path/to/image.jpg',
            inventory_count=10,
            category_id=1  # Replace with actual category ID
        )

    def test_product_name(self):
        self.assertEqual(str(self.product), 'Test Product')


class BusinessSignUpFormTest(TestCase):
    def test_valid_form(self):
        form_data = {
            'name': 'Test Owner',
            'description': 'Test Description',
            'contact_info': 'Test Contact Info',
            'contact_email': 'test@example.com',
            'contact_phone': '1234567890',
            'password': 'testpassword'
        }
        form = BusinessSignUpForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Invalid form data
        form = BusinessSignUpForm(data=form_data)
        self.assertFalse(form.is_valid())


class ProductFormTest(TestCase):
    def test_valid_form(self):
        # Create a test category for the form
        category = Category.objects.create(name='Test Category')

        form_data = {
            'owner': BusinessOwner.objects.create(name='Test Owner'),
            'name': 'Test Product',
            'description': 'Test Description',
            'price': 10.0,
            'image': 'path/to/image.jpg',
            'inventory_count': 10,
            'category': category.id
        }
        form = ProductForm(data=form_data)
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form_data = {}  # Invalid form data
        form = ProductForm(data=form_data)
        self.assertFalse(form.is_valid())


class BusinessViewsTest(TestCase):
    def test_register_business_view(self):
        response = self.client.get(reverse('Registerbusiness:register_business'))
        self.assertEqual(response.status_code, 200)
        # Add more specific tests for the view's behavior

    # Add similar test methods for other views like add_product, business_owner_login, etc.


class BusinessUrlsTest(TestCase):
    def test_dashboard_url(self):
        response = self.client.get(reverse('Registerbusiness:dashboard'))
        self.assertEqual(response.status_code, 200)
        # Add more specific tests for URL routing

    # Add similar test methods for other URLs like register_business, add_product, business_owner_login, etc.

