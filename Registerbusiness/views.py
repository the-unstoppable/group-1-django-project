from django.shortcuts import render,redirect
from django.contrib.auth import authenticate, login,logout
from django.contrib import messages
from django.urls import reverse
from .models import BusinessOwner,Product
from django.contrib.auth.decorators import login_required
from .forms import ProductForm, BusinessSignUpForm,BusinessLogInForm


def register_business(request):
    if request.method == 'POST':
        form = BusinessSignUpForm(request.POST)
        if form.is_valid():
            form.save()
            # Redirect to a success page or home page
            return redirect('Registerbusiness:dashboard')
    else:
        form = BusinessSignUpForm()
    return render(request, 'Registerbusiness/business_owner_register.html', {'form': form})

def add_product(request):
    if request.method == 'POST':
        form = ProductForm(request.POST, request.FILES)
        
        if form.is_valid():
            product = form.save(commit=False)
            
            # Check if the owner is provided in the form data
            owner_name = form.cleaned_data.get('owner_name')
            if owner_name:
                # If the owner's name is provided, create a new BusinessOwner instance
                owner = BusinessOwner.objects.create(name=owner_name)
                product.owner = owner
            else:
                # If no owner's name is provided, set a default owner or handle as needed
                # Example: Assuming there's a default owner named "Default Owner"
                default_owner = BusinessOwner.objects.get_or_create(name='Default Owner')[0]
                product.owner = default_owner
            
            
            # Check if the owner is provided in the form data
            owner_name = form.cleaned_data.get('owner_name')
            if owner_name:
                # If the owner's name is provided, create a new BusinessOwner instance
                owner = BusinessOwner.objects.create(name=owner_name)
                product.owner = owner
            else:
                # If no owner's name is provided, set a default owner or handle as needed
                # Example: Assuming there's a default owner named "Default Owner"
                default_owner = BusinessOwner.objects.get_or_create(name='Default Owner')[0]
                product.owner = default_owner
            
            product.save()
            return redirect('Registerbusiness:dashboard')
            
    else:
        form = ProductForm()
    return render(request, 'Registerbusiness/add_product.html', {'form': form})
    






def dashboard(request):
    # Add logic to fetch data for the dashboard if needed
    return render(request, 'Registerbusiness/dashboard.html')

def user_logout(request):
  logout(request)
  #redirect to the default home page
  return redirect('Registerbusiness:dashboard')


def business_owner_login(request):
    if request.method == 'POST':
        form = BusinessLogInForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                # Redirect to dashboard or another page after successful login
                return redirect('Registerbusiness:dashboard')  # Replace 'dashboard' with your dashboard URL name
            else:
                messages.error(request, 'Invalid username or password.')
    else:
        form = BusinessLogInForm()
    return render(request, 'Registerbusiness/business_owner_login.html', {'form': form})

def business_home(request):
    return render(request, 'Registerbusiness/business_home.html',{})
