

from django.db import models
from Connectify.models import Category




class BusinessOwner(models.Model):
  
    name = models.CharField(max_length=255)
    description = models.TextField()
    contact_email = models.EmailField()
    contact_phone = models.CharField(max_length=20)
    contact_info = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    
    
    def __str__(self):
        return self.name
        




class Product(models.Model):
    owner = models.ForeignKey(BusinessOwner, on_delete=models.CASCADE, related_name='business_products')
    name = models.CharField(max_length=255)
    description = models.TextField()
    price = models.DecimalField(max_digits=10, decimal_places=2)
    image = models.ImageField(upload_to='product_images/')
    inventory_count = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='products')
    
    def __str__(self):
        return self.name
