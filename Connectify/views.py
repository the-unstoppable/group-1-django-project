from django.shortcuts import render,redirect, get_object_or_404 
from .models import Product,Category,Customer, Payment, Cart,CartItem,Review
from django.core.paginator import Paginator

from django.contrib.auth import authenticate,login,logout

from django.contrib import messages

from .forms import SignUpForm,ProductForm,  PaymentForm,ReviewForm
from django.contrib.auth.decorators import login_required
from Registerbusiness.models import BusinessOwner

def review(request,id):
    product=Product.objects.get(pk=id)
    user=request.user



def index(request):
  product_objects=Product.objects.all()
  #search code
  item_name=request.GET.get('item_name')
  if  item_name!=''and item_name is not None:
    product_objects=product_objects.filter(title__icontains=item_name)

  #paginator code
  paginator=Paginator(product_objects,20)
  page = request.GET.get('page')
  product_objects=paginator.get_page(page)
    
  return render(request,'Connectify/index.html',{'product_objects':product_objects})

def categories(request):
  categories = Category.objects.all()
  context = {
    "categories": categories
  }
  return render(request,'Connectify/category.html', context)

def category_products(request, category_id):
    category = get_object_or_404(Category, pk=category_id)
    products = Product.objects.filter(category_id=category_id)
    context = {
        'category': category,
        'products': products,
    }
    return render(request, 'Connectify/category_products.html', context)
#detail view code
def detail(request, id):
    product_object = Product.objects.get(id=id)
    
    # Calculate discount percentage if discount price is available
    if product_object.discount_price:
        original_price = product_object.price
        discount_price = product_object.discount_price
        discount_percentage = ((original_price - discount_price) / original_price) * 100
    else:
        discount_percentage = None
    
    #getting all reviews
    reviews= Review.objects.filter(product=product_object)
    if request.method == 'POST':
        review_form = ReviewForm(request.POST)
        if review_form.is_valid():
            new_review = review_form.save(commit=False)
            new_review.product = product_object
            new_review.save()
            return redirect('Connectify:detail', id=id)
    else:
        review_form = ReviewForm()
    #product review form
    review_form=ReviewForm()

    context = {
        'reviews':reviews,
        'review_form': review_form,
        'product_object': product_object,
        'discount_percentage': discount_percentage,
    }

    return render(request, 'Connectify/detail.html', context)
def login_user(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to the desired page after login
            return redirect('Connectify:index')  # Replace 'dashboard' with your actual landing page URL name
        else:
            # Handle invalid login credentials
            return render(request, 'Connectify/login.html', {'error_message': 'Invalid login'})
    else:
        return render(request, 'Connectify/login.html')
#logout view
def user_logout(request):
  logout(request)
  #redirect to the default home page
  return redirect('Connectify:index')

def register_user(request):
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            customer = Customer.objects.create(
                user=user,
                first_name=form.cleaned_data['first_name'],
                last_name=form.cleaned_data['last_name'],
                email=form.cleaned_data['email'],
                phone=request.POST['phone']  # Assuming 'phone' is in your form
            )
            user = authenticate(username=user.username, password=request.POST['password1'])
            login(request, user)
            messages.success(request, "You have registered your account successfully!")
            return redirect('Connectify:index')  # Redirect to your desired URL after successful registration
    else:
        form = SignUpForm()
    return render(request, 'Connectify/register.html', {'form': form})

def about_us(request):
    # Logic for rendering the about us page
    return render(request, 'Connectify/about_us.html')  # Render the about_us.html template



# Cart Views (Optional)


@login_required
def checkout(request):
    try:
        customer = request.user
        cart = Cart.objects.get(customer=customer)
    except Cart.DoesNotExist:
        # Handle the case where the cart does not exist for the user
        # You can create a new cart here if needed
        # For simplicity, I'll render an empty cart for now
        cart = None

    return render(request, 'Connectify/checkout.html', {'cart': cart})
# Payment Views
def payment_process(request):
    # Implement payment processing logic here
    return render(request, 'Connectify/payment_process.html')

def payment_success(request):
    # Display payment success message or details
    return render(request, 'Connectify/payment_success.html')

# Address Views



def cart_summary(request):
    # get the cart
    cart = Cart(request)
    cart_products = cart.get_prods
    quantities = cart.get_quants
    totals = cart.cart_total()
    
    print("Cart Products:", cart_products)  # Debugging print statement
    
    return render(request, "Connectify/cart_view.html", {"cart_products": cart_products, "quantities": quantities, "totals": totals})



def view_businesses(request, product_id):
   # Retrieve the product object
    product = get_object_or_404(Product, pk=product_id)
    # Get the category of the product
    category = product.category
    # Retrieve businesses that have products within the same category
    businesses = BusinessOwner.objects.filter(business_products__category=category).distinct()
    return render(request, 'Connectify/view_businesses.html', {'businesses': businesses})
