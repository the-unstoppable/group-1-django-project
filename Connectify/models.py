from django.db import models
from django.contrib.auth.models import User






class Category(models.Model):
    name = models.CharField(max_length=50)
    image_url = models.CharField(max_length=2083, blank=True)

    def __str__(self):
        return self.name

RATING_CHOICES = (
    (1, '1 star'),
    (2, '2 stars'),
    (3, '3 stars'),
    (4, '4 stars'),
    (5, '5 stars'),
)

class Review(models.Model):
    user=models.ForeignKey(User,on_delete=models.CASCADE,related_name="reviews",null=True)
    product=models.ForeignKey("Product",on_delete=models.CASCADE,related_name="reviews",null=True)
    date_created=models.DateTimeField(auto_now_add=True)
    review=models.TextField(max_length=250)
    rating=models.IntegerField(choices=RATING_CHOICES, default=None)

    class Meta:
        verbose_name_plural="Reviews"
    

    def __str__(self):
        return self.product.title
    def get_rating(self):
        return self.rating
    
    
class Product(models.Model):
    title = models.CharField(max_length=200)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, related_name='connectify_products')
    price = models.FloatField()
    discount_price = models.FloatField(null=True)
    description = models.TextField()
    image = models.CharField(max_length=2083,null=True)
    
    
    def __str__(self):
        return self.title





class Customer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    phone = models.CharField(max_length=10)
    email = models.EmailField(max_length=100)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'
    
    


class Payment(models.Model):
    products = models.ManyToManyField(Product, through='CartItem')
    payment_method = models.CharField(max_length=50)
    amount_paid = models.FloatField()
    payment_date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, default='pending')


    
class Cart(models.Model):
    customer = models.OneToOneField(User, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, through='CartItem')

    def __str__(self):
        return f"Cart for {self.customer.username}"

class CartItem(models.Model):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)
    payment = models.ForeignKey(Payment, on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.quantity} x {self.product.title} in Cart for {self.cart.customer.username}"