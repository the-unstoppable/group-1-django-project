"""Defines URL patterns for Connectify."""
from django.urls import path, include
from .import views



app_name = 'Connectify'
urlpatterns = [   

    
    path('', views.index, name='index'), #home page
    path('category/', views.categories, name='category'), 
    path('category/<int:category_id>/', views.category_products, name='category_products'),
    path('<int:id>/',views.detail,name='detail'),#show  detail of a single product
    
    path('', include('django.contrib.auth.urls')),
    path('user_logout/',views.user_logout,name= "logout"),
    path('register/', views.register_user, name='register'),
    path('login/', views.login_user, name='login'),
    path('about/', views.about_us, name='about_us'),  # URL for the about us page
   
    
    path('checkout/', views.checkout, name='checkout'),
    path('payments/', views.payment_process, name='payment_process'),
    path('payments/success/', views.payment_success, name='payment_success'),
    
    
   # path('addresses/<int:address_id>/delete/', views.address_delete, name='address_delete'),
    path('cart_summary/', views.cart_summary, name='cart_summary'),
    path('view_businesses/<int:product_id>/', views.view_businesses, name='view_businesses'),
   

    ]
