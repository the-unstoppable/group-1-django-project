from django.test import TestCase, Client
from .models import Category, Product, Customer
from django.urls import reverse
from django.contrib.auth.models import User
from .forms import SignUpForm, ProductForm

class ModelsTestCase(TestCase):
    def setUp(self):
        # Create test data for Category
        self.category = Category.objects.create(name='Electronics')

        # Create test data for Product
        self.product = Product.objects.create(
            title='Laptop',
            category=self.category,
            price=1000.0,
            discount_price=900.0,
            description='A high-performance laptop',
            image='path/to/image.jpg'
        )

        # Create test data for Customer
        self.customer = Customer.objects.create(
            first_name='John',
            last_name='Doe',
            phone='1234567890',
            email='john.doe@example.com',
            password='testpassword'
        )

    def test_category_str(self):
        self.assertEqual(str(self.category), 'Electronics')

    def test_product_str(self):
        self.assertEqual(str(self.product), 'Laptop')

    def test_customer_str(self):
        self.assertEqual(str(self.customer), 'John Doe')

class ViewsTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        # Create a test user
        self.user = User.objects.create_user(username='testuser', email='test@example.com', password='testpassword')

        # Create test categories
        self.category1 = Category.objects.create(name='Electronics')
        self.category2 = Category.objects.create(name='Fashion')

        # Create a test product
        self.product = Product.objects.create(title='Test Product', category=self.category1, price=100.0, discount_price=90.0, description='Test description', image='path/to/image.jpg')

    def test_index_view(self):
        response = self.client.get(reverse('index'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/index.html')

    def test_categories_view(self):
        response = self.client.get(reverse(''))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/category.html')

    def test_category_products_view(self):
        response = self.client.get(reverse('category_products', args=[self.category1.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/category_products.html')

    def test_detail_view(self):
        response = self.client.get(reverse('detail', args=[self.product.id]))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/detail.html')

    def test_login_view(self):
        response = self.client.post(reverse('login'), {'username': 'testuser', 'password': 'testpassword'})
        self.assertEqual(response.status_code, 302)  # Redirects to index page upon successful login

    def test_logout_view(self):
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, 302)  # Redirects to index page upon logout

    def test_register_user_view(self):
        response = self.client.get(reverse('register'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/register.html')

    def test_about_us_view(self):
        response = self.client.get(reverse('about_us'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/about_us.html')

    def test_add_product_view(self):
        response = self.client.post(reverse('add_product'), {'title': 'New Product', 'category': self.category2.id, 'price': 150.0, 'discount_price': 140.0, 'description': 'New description', 'image': 'path/to/new/image.jpg'})
        self.assertEqual(response.status_code, 302)  # Redirects to index page after adding product

    def test_checkout_view(self):
        response = self.client.get(reverse('checkout'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Connectify/checkout.html')






class TestForms(TestCase):
    def test_sign_up_form_valid_data(self):
        form = SignUpForm(data={
            'username': 'testuser',
            'first_name': 'John',
            'last_name': 'Doe',
            'email': 'test@example.com',
            'password1': 'password123',
            'password2': 'password123'
        })

        self.assertTrue(form.is_valid())

    def test_sign_up_form_invalid_data(self):
        form = SignUpForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 5)  # Assuming all fields are required

    def test_product_form_valid_data(self):
        form = ProductForm(data={
            'title': 'Test Product',
            'category': 1,  # Assuming category ID
            'price': 10.99,
            'discount_price': 9.99,
            'description': 'This is a test product',
            'image': 'path/to/image.jpg'
        })

        self.assertTrue(form.is_valid())

    def test_product_form_invalid_data(self):
        form = ProductForm(data={})

        self.assertFalse(form.is_valid())
        self.assertEquals(len(form.errors), 6)  # Assuming all fields are required
        
class ConnectifyURLTests(TestCase):
    def test_index_url(self):
        response = self.client.get(reverse('Connectify:index'))
        self.assertEqual(response.status_code, 200)

    def test_category_url(self):
        response = self.client.get(reverse('Connectify:category'))
        self.assertEqual(response.status_code, 200)

    def test_category_products_url(self):
        response = self.client.get(reverse('Connectify:category_products', args=[1]))  # Assuming category_id=1 exists
        self.assertEqual(response.status_code, 200)

    def test_detail_url(self):
        response = self.client.get(reverse('Connectify:detail', args=[1]))  # Assuming product with id=1 exists
        self.assertEqual(response.status_code, 200)

    def test_user_logout_url(self):
        response = self.client.get(reverse('Connectify:logout'))
        self.assertEqual(response.status_code, 302)  # Redirects after logout

    def test_register_url(self):
        response = self.client.get(reverse('Connectify:register'))
        self.assertEqual(response.status_code, 200)

    def test_login_url(self):
        response = self.client.get(reverse('Connectify:login'))
        self.assertEqual(response.status_code, 200)

    def test_about_us_url(self):
        response = self.client.get(reverse('Connectify:about_us'))
        self.assertEqual(response.status_code, 200)

    def test_add_product_url(self):
        response = self.client.get(reverse('Connectify:add_product'))
        self.assertEqual(response.status_code, 200)

    def test_checkout_url(self):
        response = self.client.get(reverse('Connectify:checkout'))
        self.assertEqual(response.status_code, 200)
