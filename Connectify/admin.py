from django.contrib import admin
from .models import Category,Product,Customer,Payment,Cart,CartItem

admin.site.register(Category)
admin.site.register(Product)
admin.site.register(Customer)

admin.site.register(Payment)

admin.site.register(Cart)
admin.site.register(CartItem)

