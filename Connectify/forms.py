import stripe
from django import forms
from django.contrib.auth.models import User
from .models import Product,Customer, Payment
from django.contrib.auth.forms import UserCreationForm
from stripe import Review
from Connectify.models import Review

class ReviewForm(forms.ModelForm):
    review=forms.CharField(widget=forms.Textarea(attrs={'placeholder':"write review"}))
    class Meta:
        model=Review
        fields=['review','rating']


class SignUpForm(UserCreationForm):
    email = forms.EmailField(label="", widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Email Address'}))
    first_name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'First Name'}))
    last_name = forms.CharField(label="", max_length=100, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Last Name'}))
    phone = forms.CharField(label="", max_length=10, widget=forms.TextInput(attrs={'class':'form-control', 'placeholder':'Phone Number'}))  # Add this line

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', 'phone')  # Include 'phone' field in fields attribute

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)

        self.fields['username'].widget.attrs['class'] = 'form-control'
        self.fields['username'].widget.attrs['placeholder'] = 'User Name'
        self.fields['username'].label = ''
        self.fields['username'].help_text = '<span class="form-text text-muted"><small>Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.</small></span>'
class ProductForm(forms.ModelForm):
			class Meta:
				model=Product
				fields=['title','category','price','discount_price','description','image']




class PaymentForm(forms.ModelForm):
    class Meta:
        model = Payment
        fields = ['products', 'payment_method', 'amount_paid', 'status']


class CartItemForm(forms.Form):
    quantity = forms.IntegerField(min_value=1, initial=1)