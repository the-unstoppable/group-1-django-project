from django.urls import path, include
from. import views
from rest_framework.routers import DefaultRouter

router = DefaultRouter()

router.register("products", views.ProductsViewSet)
router.register("categories", views.CategoriesViewSet)

urlpatterns = [
    path("", include(router.urls)),
    # path('products',views.api_products),
    # path('products',views.ApiProducts.as_view()),
    # path('categories',views.ApiCategories.as_view()),
    # path('product/<str:pk>',views.ApiProduct.as_view()),
    # path('category/<str:pk>',views.ApiCategory.as_view()),
]