from rest_framework import serializers
from Connectify.models import Product,Category

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model=Category
        fields=["name","id"]

class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields=["title","id", "category","price","description"]

    