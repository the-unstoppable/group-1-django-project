from django.shortcuts import render,get_object_or_404
from rest_framework.decorators import api_view
from .serializers import ProductSerializer,CategorySerializer
from Connectify.models import Product,Category
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from api import serializers
from rest_framework.generics import ListCreateAPIView,RetrieveUpdateDestroyAPIView#helps us write lesser code
from rest_framework.viewsets import ModelViewSet#when we have similar querysets and serializers of two functions
# Note:When using viewsets,we dont use urls,rather,we use routers
from django_filters.rest_framework import DjangoFilterBackend
from api.filters import ProductFilter
from rest_framework.filters import SearchFilter,OrderingFilter
from rest_framework.pagination import PageNumberPagination


class ProductsViewSet(ModelViewSet):
    queryset=Product.objects.all()
    serializer_class=ProductSerializer



    filter_backends=[DjangoFilterBackend,SearchFilter,OrderingFilter]
    filterset_class=ProductFilter#fields we want to filter with
    search_fields=['title','description']
    ordering_fields=['price']
    pagination_class=PageNumberPagination

    
    

class CategoriesViewSet(ModelViewSet):
    queryset=Category.objects.all()
    serializer_class=CategorySerializer
    







#CLASS-BASED VIEWS
# class ApiProduct(RetrieveUpdateDestroyAPIView):
#     queryset=Product.objects.all()
#     serializer_class=ProductSerializer

# class ApiCategory(RetrieveUpdateDestroyAPIView):
#     queryset=Category.objects.all()
#     serializer_class=CategorySerializer
    # def get(self,request,pk):
    #     category=get_object_or_404(Category,id=pk)
    #     serializer=CategorySerializer(category)
    #     return Response(serializer.data)

    # def put(self,request,pk):
    #     category=get_object_or_404(Category,id=pk)
    #     serializer=ProductSerializer(category,data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data)

    # def delete(self,request,pk):
    #     category=get_object_or_404(Category,id=pk)
    #     category.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)









#FUNCTION BASED VIEWS

# @api_view(['GET','POST'])
# def api_products(request):
#     if request.method=='GET':
#         products=Product.objects.all()
#         serializer=ProductSerializer(products,many=True)
#         return Response(serializer.data)

#     if request.method=='POST':
#         serializer=ProductSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)


# @api_view(['GET','PUT','DELETE'])
# def api_product(request,pk):
#     product=get_object_or_404(Product,id=pk)
#     if request.method=="GET": 
#         serializer=ProductSerializer(product)
#         return Response(serializer.data)

#     if request.method=='PUT':
#         serializer=ProductSerializer(product,data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)

#     if request.method=='DELETE':
#         product.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# @api_view(['GET','POST'])
# def api_categories(request):
#     if request.method=='GET':
#        categories = Category.objects.all()
#        serializer=CategorySerializer(categories,many=True)
#        return Response(serializer.data)

#     if request.method=='POST':
#         serializer=CategorySerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         serializer.save()
#         return Response(serializer.data)


# @api_view(['GET','PUT','DELETE'])
# def api_category(request,pk):
#     category=get_object_or_404(Category,id=pk)
#     if request.method=="GET":
#         serializer=CategorySerializer(category)
#         return Response(serializer.data)

    # if request.method=="PUT":
    #     serializer=ProductSerializer(category,data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #     serializer.save()
    #     return Response(serializer.data)

    # if request.method=='DELETE':
    #     category.delete()
    #     return Response(status=status.HTTP_204_NO_CONTENT)


# Create your views here.
